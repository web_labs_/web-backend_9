import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Column } from './entities/column.entity';

@Injectable()
export class ColumnsService extends TypeOrmCrudService<Column> {
  constructor(@InjectRepository(Column) repo) {
    super(repo);
  }
}
