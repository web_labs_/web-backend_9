import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateColumnDto {
  @ApiProperty({description: 'The name of column'})
  @IsString()
  name: string;
}
